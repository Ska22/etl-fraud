#!/usr/bin/python


import os
import pandas as pd
import jaydebeapi
from sqlalchemy import types
import time
from py_scripts import clients,terminals,transactions,pssp_black,clients,accounts,cards,fraud
conn = jaydebeapi.connect(
"oracle.jdbc.driver.OracleDriver",
"jdbc:oracle:---------",
["----","----k"],
"/home/demipt2/ojdbc8.jar"
)
conn.jconn.setAutoCommit(False)
curs = conn.cursor()

#Пустая папка, сюда должны перемещаться отработанные файлы
if not os.path.isdir("archive"):
     os.mkdir("archive")

LOAD_FILES = os.listdir('./data')  # т.к. константа, то имя переменной с большой буквы
LOAD_FILES.sort()  # т.к. сортируем по возрастанию, чтобы взять первый элемент для загрузки
load_date = " "

if len(LOAD_FILES) == 0:
    print('\033[31mFiles not found\033[0m')
if not ('transactions_' + load_date + '.txt') in os.listdir('./data'):
    print('\033[31mFile transactions not found\033[0m')
if not ('passport_blacklist_' + load_date + '.xlsx') in os.listdir('./data'):
    print('\033[31mFile passport_blacklist not found\033[0m')
if not ('terminals_' + load_date + '.xlsx') in os.listdir('./data'):
    print('\033[31mFile terminals not found\033[0m')



conn.commit()
curs.close()
conn.close()
